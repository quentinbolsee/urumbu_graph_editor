import serial
import time


class Node:
    def __init__(self, name, input_names=(), output_names=(), input_init=None):
        self.name = name
        self.n_input = len(input_names)
        self.n_output = len(output_names)

        # start with default values
        self.input_names = input_names
        self.input_connections = [None for _ in range(self.n_input)]
        self.input_values = [None for _ in range(self.n_input)]
        self.output_names = output_names
        self.output_connections = [set() for _ in range(self.n_output)]

        self.input_lookup = {name: i for i, name in enumerate(input_names)}
        self.output_lookup = {name: i for i, name in enumerate(output_names)}

        if input_init is not None:
            for name, value in input_init.items():
                self.input_values[self.input_lookup[name]] = value

    def is_input_connected(self, i):
        return self.input_connections[i] is not None

    def is_output_connected(self, i):
        return len(self.output_connections[i]) > 0

    def connect_input(self, i, other, other_i):
        self.input_connections[i] = (other, other_i)

    def disconnect_input(self, i):
        self.input_connections[i] = None

    def connect_output(self, i, other, other_i):
        output_connections = self.output_connections[i]  # type: set
        output_connections.add((other, other_i))

    def disconnect_output(self, i, other, other_i):
        output_connections = self.output_connections[i]  # type: set
        output_connections.remove((other, other_i))

    def input(self, i):
        if self.input_connections[i] is not None:
            # update if we can
            other, other_i = self.input_connections[i]
            val = other.output(other_i)
            if val is not None:
                self.input_values[i] = val

        return self.input_values[i]

    def output(self, i):
        raise NotImplementedError()


class UrumbuNode(Node):
    def __init__(self, ser, name, input_names=(), output_names=(), input_init=None):
        self.ser = ser
        super().__init__(name=name, input_names=input_names, output_names=output_names, input_init=input_init)

    def close(self):
        self.ser.close()

    def write(self, c):
        self.ser.write(c)

    def read(self, n=None):
        if n is None:
            return self.ser.read()
        else:
            return self.ser.read(n)


class UrumbuNode10bitOutput(UrumbuNode):
    def __init__(self, ser, name, output_name="output"):
        super().__init__(ser, name=name, output_names=(output_name,))

    def output(self, i):
        try:
            if i != 0:
                return None

            self.write(b'?')
            buffer = self.read(2)
            v = int.from_bytes(buffer, byteorder='little')
            return v

        except serial.SerialException as e:
            return None


class UrumbuStepper(UrumbuNode):
    pass


class UrumbuServo(UrumbuNode):
    pass


class UrumbuCapa(UrumbuNode10bitOutput):
    def __init__(self, ser):
        super().__init__(ser, name=f"Capacitor at {ser.port}", output_name="capacitance")


class UrumbuPot(UrumbuNode10bitOutput):
    def __init__(self, ser):
        super().__init__(ser, name=f"Potentiometer at {ser.port}", output_name="level")


class UrumbuDistance(UrumbuNode10bitOutput):
    def __init__(self, ser):
        super().__init__(ser, name=f"Dist. sensor at {ser.port}", output_name="distance")


class UrumbuPressure(UrumbuNode10bitOutput):
    def __init__(self, ser):
        super().__init__(ser, name=f"Press. sensor at {ser.port}", output_name="pressure")


class Graph:
    def __init__(self):
        self.nodes = {}

    def __getitem__(self, item):
        return self.nodes[item]

    def reset(self):
        self.nodes = {}

    def add_node(self, node_id, node):
        self.nodes[node_id] = node

    def delete_node(self, node_id):
        if node_id not in self.nodes:
            return
        node = self.nodes[node_id]
        for conn_info in node.input_connections:
            if conn_info is None:
                continue
            other, other_i = conn_info
            other.disconnect_output(other_i)
        for conn_set in node.output_connections:
            for conn_info in conn_set:
                if conn_info is None:
                    continue
                other, other_i = conn_info
                other.disconnect_input(other_i)
        self.nodes.pop(node_id)

    def connect(self, node_out_id, node_out_i, node_in_id, node_in_i):
        if node_out_id not in self.nodes or node_in_id not in self.nodes:
            return

        node_out = self.nodes[node_out_id]
        node_in = self.nodes[node_in_id]

        node_out.connect_output(node_out_i, node_in, node_in_i)
        node_in.connect_input(node_in_i, node_out, node_out_i)

    def disconnect(self, node_out_id, node_out_i, node_in_id, node_in_i):
        if node_out_id not in self.nodes or node_in_id not in self.nodes:
            return

        node_out = self.nodes[node_out_id]
        node_in = self.nodes[node_in_id]

        node_out.disconnect_output(node_out_i, node_in, node_in_i)
        node_in.disconnect_input(node_in_i)


sig_class = {
    "0000": UrumbuNode,
    "0001": UrumbuStepper,
    "0002": UrumbuServo,
    "0003": UrumbuCapa,
    "0004": UrumbuPot,
    "0005": UrumbuDistance,
    "0006": UrumbuPressure
}


def main():
    try:
        # ser = serial.Serial("COM47", baudrate=115200, timeout=0.1)
        # ser = serial.Serial("COM48", baudrate=115200, timeout=0.1)
        ser = serial.Serial("COM49", baudrate=115200, timeout=0.1)

        node = UrumbuPot(ser)

        # for i in range(10)

        try:
            while True:
                t = time.perf_counter()
                val = node.output(0)
                dt = time.perf_counter() - t

                print(f"{val:03d}, {dt:6.5f}")

                time.sleep(0.1)

        finally:
            node.close()

    except serial.serialutil.SerialException:
        pass


if __name__ == "__main__":
    main()
