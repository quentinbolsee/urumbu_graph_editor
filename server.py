import time
import websockets
import json
import asyncio
import logging
from urllib.parse import quote, unquote
import serial
import serial.tools.list_ports

import graph_engine
import urumbu_process


logging.getLogger().setLevel(logging.INFO)

state_nodes = {}
state_ports = set()
state_connections = []
state_websocket = None

state_graph = graph_engine.Graph()
state_ports_ignore = {"COM40", "COM41", "COM4"}


def try_open(port, baudrate=115200):
    ser = serial.Serial(port, baudrate=baudrate, timeout=0.5)

    ser.write(b"@")
    sig_bytes = ser.read(4)
    if sig_bytes is None:
        return None

    sig = sig_bytes.decode("ascii")
    if sig in graph_engine.sig_class:
        cls = graph_engine.sig_class[sig]
        return cls(ser)


async def handler(websocket):
    global state_websocket

    state_websocket = websocket

    await send_current_graph()
    async for message in websocket:
        try:
            msg_obj = json.loads(message)

            logging.info(f"Received: {msg_obj}")

            action = msg_obj["action"]
            if action == "add_connection":
                node_out_id, _, node_out_i = msg_obj["from"].split(":")
                node_in_id, _, node_in_i = msg_obj["to"].split(":")
                node_out_i = int(node_out_i)
                node_in_i = int(node_in_i)
                await add_connection(node_out_id, node_out_i, node_in_id, node_in_i, sync=False)
            elif action == "delete_connection":
                node_out_id, _, node_out_i = msg_obj["from"].split(":")
                node_in_id, _, node_in_i = msg_obj["to"].split(":")
                node_out_i = int(node_out_i)
                node_in_i = int(node_in_i)
                await delete_connection(node_out_id, node_out_i, node_in_id, node_in_i, sync=False)

            # decode_action(msg_obj)
        except json.JSONDecodeError:
            logging.error(f"Error decoding msg: {message}")

    state_websocket = None


async def update_ports():
    global state_websocket, state_graph, state_ports_ignore

    ports = serial.tools.list_ports.comports()

    ports_seen = set()
    ports_known = set()

    for port in state_ports:
        ports_known.add(port)

    for port, desc, hwid in sorted(ports):
        if port in state_ports_ignore:
            continue
        ports_seen.add(port)

    ports_new = ports_seen - ports_known
    ports_delete = ports_known - ports_seen

    for port in ports_new:
        try:
            obj = try_open(port)
        except serial.serialutil.SerialException as e:
            continue

        if obj is None:
            state_ports_ignore.add(port)
            continue

        logging.info(f"Adding: {port}, {type(obj)}")
        await add_node(port, obj)
        state_ports.add(port)

    for port in ports_delete:
        obj = state_graph.nodes[port]
        obj.close()
        state_ports.remove(port)

        logging.info(f"Deleting: {port}, {type(obj)}")
        await delete_node(port)


async def send_reset_graph():
    if state_websocket is None:
        return

    await state_websocket.send(json.dumps({"action": "reset"}))


async def send_current_graph():
    # for good measure
    await send_reset_graph()

    for node_id, node in state_graph.nodes.items():
        await send_add_node(node_id, node)

    for node_id, node in state_graph.nodes.items():
        for i, conn_info in enumerate(node.input_connections):
            if conn_info is None:
                continue
            other, other_i = conn_info

            other_id = None
            for other_id_iter, other_iter in state_graph.nodes.items():
                if other == other_iter:
                    other_id = other_id_iter
                    break

            if other_id is None:
                continue

            await send_add_connection(other_id, other_i, node_id, i)


async def add_node(node_id, node, sync=True):
    state_graph.add_node(node_id, node)

    if sync and state_websocket is not None:
        await send_add_node(node_id, node)


async def send_add_node(node_id, node):
    msg = {
        "action": "add_node",
        "id": node_id,
        "name": node.name,
        "inputs": node.input_names,
        "outputs": node.output_names
    }
    await state_websocket.send(json.dumps(msg))


async def delete_node(node_id, sync=True):
    state_graph.delete_node(node_id)

    if sync and state_websocket is not None:
        await send_delete_node(node_id)


async def send_delete_node(node_id):
    msg = {
        "action": "delete_node",
        "id": node_id,
    }
    await state_websocket.send(json.dumps(msg))


async def add_connection(node_out_id, node_out_i, node_in_id, node_in_i, sync=True):
    state_graph.connect(node_out_id, node_out_i, node_in_id, node_in_i)

    if sync and state_websocket is not None:
        await send_add_connection(node_out_id, node_out_i, node_in_id, node_in_i)


async def send_add_connection(node_out_id, node_out_i, node_in_id, node_in_i):
    msg = {
        "action": "add_connection",
        "from": f"{node_out_id}:out:{node_out_i}",
        "to": f"{node_in_id}:in:{node_in_i}",
    }
    await state_websocket.send(json.dumps(msg))


async def delete_connection(node_out_id, node_out_i, node_in_id, node_in_i, sync=True):
    state_graph.disconnect(node_out_id, node_out_i, node_in_id, node_in_i)

    if sync and state_websocket is not None:
        await send_delete_connection(node_out_id, node_out_i, node_in_id, node_in_i)


async def send_delete_connection(node_out_id, node_out_i, node_in_id, node_in_i):
    msg = {
        "action": "delete_connection",
        "from": f"{node_out_id}:out:{node_out_i}",
        "to": f"{node_in_id}:in:{node_in_i}",
    }
    await state_websocket.send(json.dumps(msg))


class MathNode(graph_engine.Node):
    def __init__(self, name, func, name_output):
        self.func = func
        super().__init__(name, ("input", ), (name_output, ), input_init={"input": 0})

    def output(self, i):
        if i != 0:
            return None

        return self.func(self.input(0))


async def loop_ports():
    while True:
        await update_ports()
        await asyncio.sleep(0.5)


class MachineNode(graph_engine.Node):
    def __init__(self, name, input_names=(), input_init=None):
        super().__init__(name, input_names, input_init=input_init)


async def loop_machine():
    m = MachineNode("Machine", input_names=("x", "y", "z"), input_init={"x": 0, "y": 0, "z": 0})

    state_graph.add_node("urumbu", m)

    modules_config = {
        "a": {
            "type": "stepper",
            "port": "COM40",
            "baudrate": 115200,
            "axis": 0,
            "steps_per_unit": 8 * 200 / 32,  # 32 mm / turn
            "reverse": True
        },
        "b": {
            "type": "stepper",
            "port": "COM4",
            "baudrate": 115200,
            "axis": 1,
            "steps_per_unit": 8 * 200 / 32,  # 32 mm / turn
            "reverse": True
        },
        "x": {
            "type": "stepper",
            "port": "COM41",
            "baudrate": 115200,
            "axis": 2,
            "steps_per_unit": 8 * 200 / 32,  # 32 mm / turn
            "reverse": False
        },
    }

    pos_array, pos_lock = urumbu_process.init_machine(modules_config, urumbu_process.transform_corexy)

    while True:
        x = 75 * m.input(0) / 1023
        y = 75 * m.input(1) / 1023
        z = 75 * m.input(2) / 1023

        pos_lock.acquire()
        pos_array[0] = x
        pos_array[1] = y
        pos_array[2] = z
        pos_lock.release()

        await asyncio.sleep(0.05)


async def launch_server():
    async with websockets.serve(handler, 'localhost', 4000):
        await asyncio.Future()  # run forever


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.create_task(loop_machine())
    loop.create_task(launch_server())
    loop.create_task(loop_ports())
    loop.run_forever()
