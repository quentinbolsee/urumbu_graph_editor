import re
import serial
import time
import multiprocessing
import logging
import argparse
import math
import os
import numpy as np

import graph_engine


BAUDRATE_DEFAULT = 115200


class Module:
    def __init__(self, port, baudrate=BAUDRATE_DEFAULT):
        self.port = None
        self.baudrate = baudrate
        try:
            self.port = serial.Serial(port, baudrate)
        except serial.SerialException:
            logging.error(f"Cannot connect to {port}")

    @property
    def connected(self):
        return self.port is not None

    def write(self, txt):
        self.port.write(txt)

    def close(self):
        self.port.close()

    def pressed(self, nc=True):
        self.write(b"?")
        r = self.port.read(1)
        if nc:
            return r == b"1"
        else:
            return r == b"0"


class Stepper(Module):
    def __init__(self, steps_per_unit, port, baudrate=BAUDRATE_DEFAULT, reverse=False):
        super().__init__(port, baudrate)
        self.steps = 0
        self.reverse = reverse
        self.steps_per_unit = steps_per_unit

    def step(self, forward):
        self.steps += 1 if forward else -1
        if self.reverse:
            forward = not forward
        self.write(b"f" if forward else b"r")


class Servo(Module):
    def __init__(self, pulse_min, pulse_max, port, baudrate=BAUDRATE_DEFAULT):
        self.pulse_min = pulse_min
        self.pulse_max = pulse_max
        super().__init__(port, baudrate)
        self.delay_us = 0

    def pulse(self, delay_us):
        self.write(delay_us.to_bytes(2, byteorder='little'))

    def fraction(self, f):
        p = int(self.pulse_min + (self.pulse_max - self.pulse_min) * f)
        self.pulse(p)

    def pressed(self, nc=True):
        self.pulse(65535)
        r = self.port.read(1)
        if nc:
            return r == b"1"
        else:
            return r == b"0"


class RGB(Module):
    def __init__(self, port, baudrate=BAUDRATE_DEFAULT):
        super().__init__(port, baudrate)

    def write_rgb(self, red, green, blue):
        rgb = (blue << 16) | (green << 8) | red
        self.write(rgb.to_bytes(3, byteorder="little"))


class Spindle(Module):
    def __init__(self, port, baudrate=BAUDRATE_DEFAULT):
        super().__init__(port, baudrate)

    def set_rpm(self, rpm):
        pass

    def set_on(self, on):
        if on:
            self.write(b"1")
        else:
            self.write(b"0")


class Action:
    def __iter__(self):
        return [self].__iter__()


class HomingAction(Action):
    def __init__(self, axis, name, pos, feedrate, nc=True):
        self.axis = axis
        self.name = name
        self.pos = np.array(pos)
        self.feedrate = feedrate
        self.nc = nc


def transform_corexy(pos, pos_transform):
    pos_transform[:] = pos[:]
    pos_transform[0] = (pos[0] + pos[1])/2
    pos_transform[1] = (pos[0] - pos[1])/2


def modules_manager(modules_config, target_pos, pos_lock, pos_transformer=None):
    logging.info("start loop")

    modules = {}

    modules_axis = {}

    for name, config in modules_config.items():
        if config["type"] == "stepper":
            obj = Stepper(config["steps_per_unit"],
                          config["port"],
                          config["baudrate"],
                          reverse=config.get("reverse", False))
            modules[name] = obj
            if "axis" in config:
                modules_axis[config["axis"]] = obj
        elif config["type"] == "servo":
            modules[name] = Servo(config["pulse_min"],
                                  config["pulse_max"],
                                  config["port"],
                                  config["baudrate"])
        elif config["type"] == "rgb":
            modules[name] = RGB(config["port"], config["baudrate"])
        elif config["type"] == "spindle":
            modules[name] = Spindle(config["port"], config["baudrate"])

    class MotorTicker:
        def __init__(self, n_axis, max_speed, max_acc):
            self.n_axis = n_axis

            self.motors_steps = np.zeros((self.n_axis,))

            self.min_speed = 0.2
            self.max_speed = max_speed
            self.max_acc = max_acc

            self.state_pos = np.zeros((self.n_axis,))
            self.state_speed = np.zeros((self.n_axis,))
            # self.count = 0
            # self.count2 = 0
            # self.count_max = 50

        def tick_motor(self, dt):
            if dt == 0:
                return

            # self.count += 1

            pos_lock.acquire()
            target_pos_np = np.array(target_pos)
            pos_lock.release()

            diff = target_pos_np - self.state_pos

            for i in range(self.n_axis):
                dist = abs(diff[i])

                norm_speed = abs(self.state_speed[i])
                if norm_speed < 1e-6:
                    norm_speed = 0

                dist_brake = (norm_speed * norm_speed) / (2*self.max_acc)

                if dist < 1e-6 or dist < norm_speed * dt:
                    # reached destination right now
                    self.state_pos[i] = target_pos_np[i]
                    self.state_speed[i] = 0
                else:
                    # print(self.state_speed, self.state_pos)
                    vec = np.sign(diff)[i]

                    if (vec * self.state_speed[i]) < 0:
                        # flip to the right direction
                        # print("flip")
                        self.state_speed[i] += vec * self.max_acc * dt
                    else:
                        if dist < dist_brake:
                            # decelerate
                            # if self.count == self.count_max:
                            #     print(f"decel {self.state_speed[i]} {self.count2:05d}")
                            if norm_speed != 0:
                                if norm_speed <= self.max_acc * dt:
                                    # minimal speed reached right now
                                    self.state_speed[i] = 0
                                else:
                                    self.state_speed[i] -= vec * self.max_acc * dt
                        else:
                            if norm_speed < self.max_speed:
                                # accelerate
                                # if self.count == self.count_max:
                                #     print(f"accel {self.state_speed[i]} {self.count2:05d}")
                                if self.max_speed - norm_speed <= self.max_acc * dt:
                                    # max speed reached right now
                                    self.state_speed[i] = vec * self.max_speed
                                else:
                                    self.state_speed[i] += vec * self.max_acc * dt

                    self.state_pos[i] += self.state_speed[i] * dt

            # tick those motors!
            if pos_transformer is None:
                self.motors_steps[:] = self.state_pos[:]
            else:
                pos_transformer(self.state_pos[:], self.motors_steps)
            for j in range(self.n_axis):
                m = modules_axis[j]
                s = int(self.motors_steps[j] * m.steps_per_unit)
                if m.steps < s:
                    m.step(True)
                elif m.steps > s:
                    m.step(False)

    m = MotorTicker(len(modules_axis), 65, 350)

    dt = 0
    while True:
        t = time.perf_counter()
        m.tick_motor(dt)
        dt = time.perf_counter() - t


def init_machine(modules_config, pos_transform=None):
    multiprocessing.set_start_method('spawn')

    n = len(modules_config)

    pos_array = multiprocessing.Array("f", [0 for i in range(n)])
    pos_lock = multiprocessing.Lock()

    p1 = multiprocessing.Process(target=modules_manager, args=(modules_config, pos_array, pos_lock, pos_transform))
    p1.start()

    return pos_array, pos_lock
