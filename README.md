# Urumbu graph editor

## Installation

```
python -m pip install -r requirements.txt
```

## Running

```
python server.py
```

## Frontend

This project is to be used with this frontend:
https://github.com/leomcelroy/software-defined-machines-graphs
